import numpy as np
import cv2

def hist_equalization(img):
    hist = [0] * 256

    # frekuensi setiap value dalam setiap pixel
    rows, cols = img.shape
    for i in range(rows):
        for j in range(cols):
            hist[img[i,j]] += 1 

    accu = np.array([sum(hist[:i+1]) for i in range(len(hist))]) # akumulasi frekuensi setiap value
    accu = accu / (rows * cols) # rasio akumulasi frekuensi dengan jumlah pixel
    accu *= 255

    out = img.copy()
    for i in range(rows):
        for j in range(cols):
            out[i,j] = accu[img[i,j]] # ubah value setiap pixel sesuai array accumulative

    return out
