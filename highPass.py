#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 15 01:15:01 2019

@author: bntng01
"""


from PIL import Image
import numpy as np
import math

#Inisiasi list untuk menyimpan pixel sementara
listPixel = []

#inisiasi nama file yang akan diolah
filename='lena.jpg'

#mengubah file menjadi graysclae image
img = Image.open(filename).convert('L')
#new_filename = "gray_" + filename
#img.save(new_filename)

#menginisasi nilai lebar dan panjang gambar
width,height = img.size

#membuat array berisi 0
arnex = np.zeros((width+1,height+1))

#membuat image baru yang akan menjadi image hasil filter
newImg = Image.new("L",(width,height), "white")

#proses konvolusi
for x in range(1,width-1):
    for y in range(1,height-1):
        #inisiasi nilai gx dan gy
        Gx = 0
        Gy = 0
        
        #proses perkalian kernel dengan pixel
        intensity = img.getpixel((x-1, y-1))
        Gx += -intensity
        Gy += -intensity
        
        intensity = img.getpixel((x-1, y))
        Gx += -2 * intensity
        
        intensity = img.getpixel((x-1, y+1))
        Gx += -intensity
        Gy += intensity
        
        intensity = img.getpixel((x, y-1))
        Gy += -2 * intensity
        
        intensity = img.getpixel((x, y+1))
        Gy += 2 * intensity
         
         
        intensity = img.getpixel((x+1, y-1))
        Gx += intensity
        Gy += -intensity
        
        intensity = img.getpixel((x+1, y))
        Gx += 2 * intensity
        
        intensity = img.getpixel((x+1, y+1))
        Gx += intensity
        Gy += intensity
        
        #menghitung nilai pixel yang baru
        length = math.sqrt((Gx * Gx) + (Gy * Gy))
        #meletakkan nilai pixel baru ke array
        arnex[x,y] = length

#mencari nilai pixel tertinggi untuk normalisasi        
normal = np.max(arnex)

#proses pembuatan citra hasil filter
for x in range(1,width-1):
    for y in range(1,height-1):
            newImg.putpixel((x,y),int(arnex[x,y]/normal * 255))

#menampilkan citra hasil filter
newImg.show()
       
        
        