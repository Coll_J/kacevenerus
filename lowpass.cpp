#include <bits/stdc++.h>
#include <opencv2/opencv.hpp>

cv::Mat createKernel(int size)
{
    cv::Mat_<double> tempKernel(size,size);
    for( int i = 0; i < tempKernel.rows; ++i)
    {
        for( int j = 0; j < tempKernel.cols; ++j )
        {
            tempKernel(i,j) = 1./(size*size);
        }
    }
    std::cout << "kernel = \n "<< tempKernel <<"\n\n";
    return tempKernel;
}

void convolution(cv::Mat imgOriginal, cv::Mat Kernel)
{
    cv::Mat imgConvo;
    imgConvo = cv::Mat(imgOriginal.rows, imgOriginal.cols, CV_64FC3);
    for (int x=(Kernel.rows-1)/2; x<imgOriginal.rows-((Kernel.rows-1)/2); x++)
    {
        for (int y=(Kernel.cols-1)/2; y<imgOriginal.cols-((Kernel.cols-1)/2); y++)
        {
            double comp_1=0;
            double comp_2=0;
            double comp_3=0;
            for (int u=-(Kernel.rows-1)/2; u<=(Kernel.rows-1)/2; u++)
            {
                for (int v=-(Kernel.cols-1)/2; v<=(Kernel.cols-1)/2; v++)
                {
                    comp_1 = comp_1 + ( imgOriginal.at<cv::Vec3d>(x+u,y+v)[0] * (Kernel.at<double>(u + ((Kernel.rows-1)/2) ,v + ((Kernel.cols-1)/2))));
                    comp_2 = comp_2 + ( imgOriginal.at<cv::Vec3d>(x+u,y+v)[1] * (Kernel.at<double>(u + ((Kernel.rows-1)/2), v + ((Kernel.cols-1)/2))));
                    comp_3 = comp_3 + ( imgOriginal.at<cv::Vec3d>(x+u,y+v)[2] * (Kernel.at<double>(u + ((Kernel.rows-1)/2), v + ((Kernel.cols-1)/2))));
                }
            }
            imgConvo.at<cv::Vec3d>(x-((Kernel.rows-1)/2),y-(Kernel.cols-1)/2)[0] = comp_1;
            imgConvo.at<cv::Vec3d>(x-((Kernel.rows-1)/2),y-(Kernel.cols-1)/2)[1] = comp_2;
            imgConvo.at<cv::Vec3d>(x-((Kernel.rows-1)/2),y-(Kernel.cols-1)/2)[2] = comp_3;
        }
    }
    imgConvo.convertTo(imgConvo, CV_8UC3);
    // cv::namedWindow("convolution - manual", CV_WINDOW_NORMAL ); 
    imshow("convolution - manual", imgConvo);
}

int main(int argc, char** argv)
{    
    cv::Mat imgOriginal = cv::imread(argv[1]);

    if(! imgOriginal.data )
    {
        std::cout <<  "Could not open or find the image" << std::endl ;
        return -1;
    }

    // cv::namedWindow("Original", CV_WINDOW_NORMAL ); 
    imshow("Original", imgOriginal);
    imgOriginal.convertTo(imgOriginal, CV_64FC3);

    int kernelSize;
    std::cout<<"Enter kernel size : "; std::cin>>kernelSize;

    // Defining the kernel
    cv::Mat Kernel = createKernel(kernelSize);

    //convolution
    convolution(imgOriginal, Kernel);

    cv::waitKey();
    return 0;
}