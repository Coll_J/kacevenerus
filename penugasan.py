import cv2
import numpy as np

from matplotlib import pyplot as plt
from eqhist import hist_equalization

img = cv2.imread('transcendence.jpg', 0)

scale = 30
width = int(img.shape[1] * scale / 100)
height = int(img.shape[0] * scale / 100)
img = cv2.resize(img, (width, height))

out = hist_equalization(img) # histogram equalization

cv2.imshow('input', img)
plt.hist(img.flatten(), 256, [0,256])
# plt.show()

cv2.imshow('output', out)
plt.hist(out.flatten(), 256, [0,256])
plt.show()
